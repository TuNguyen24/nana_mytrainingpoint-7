const nodemailer = require('nodemailer');
var mysql = require('mysql');
var config = require('config');
var hbs = require('nodemailer-express-handlebars');

var database = config.get('database');
var email = config.get('email');
var connection = mysql.createConnection(database);

SendMail = function(mailTo,tieuDe,template,context,cb){
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
            user: email.user, // generated ethereal user
            pass: email.pass  // generated ethereal password
        }
    });

    transporter.use('compile',hbs({
        viewPath: 'views',
        extName: '.ejs'
    }))

    // setup email data with unicode symbols
    let mailOptions = {
        from: '"Nana TrainingPoint"',
        to: mailTo,
        subject: tieuDe,
        template: template,
        context: context
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        return cb(error,info);
    });
}

exports.GuiMailNhacSinhVienHDSapToi = function(){
    connection.query('select hd.Ten,sv.Email,hd.DiaDiemToChuc,hd.NgayToChuc from thamgiahoatdong tg join hoatdong hd on tg.MaHD=hd.MaHD join sinhvien sv on tg.MSSV = sv.MSSV  where adddate(current_date(),3) >= NgayToChuc and NgayToChuc > current_date();'
    ,function(err,data){
        if(err){
            console.log(err);
            return;
        }
        else{
            for(var i=0;i<data.length;i++){
                SendMail(data[i].Email,'Thông báo hoạt động sắp tới','mail/ThongBaoHoatDong',
                {Ten: data[i].Ten,Ngay:data[i].NgayToChuc.toLocaleDateString(),DiaDiem:data[i].DiaDiemToChuc},function(err,info){
                    if(err){
                        console.log(err);
                        return;
                    }
                    else{
                        console.log(info);
                        return;
                    }
                });
            }
        }
    })
}